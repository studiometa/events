'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.on = on;
exports.once = once;
exports.off = off;

var _foreach = require('foreach');

var _foreach2 = _interopRequireDefault(_foreach);

var _is = require('is');

var is = _interopRequireWildcard(_is);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Add en event listener
 * @param  {String|HTMLElement|HTMLCollection|NodeList} target
 * @param  {String} type
 * @param  {Function} handler
 * @return {void}
 */
function on(target, type, handler) {
  event('addEventListener', target, type, handler);
}

/**
 * Add an event listener which
 * will be triggered only once
 * @param  {String|HTMLElement|HTMLCollection|NodeList} target
 * @param  {String} type
 * @param  {Function} handler
 * @return {void}
 */
function once(target, type, handler) {

  function detach() {
    off(target, type, handler);
    off(target, type, detach);
  }

  on(target, type, handler);
  on(target, type, detach);
}

/**
 * Remove an event listener
 * @param  {String|HTMLElement|HTMLCollection|NodeList} target
 * @param  {String} type
 * @param  {Function} handler
 * @return {void}
 */
function off(target, type, handler) {
  event('removeEventListener', target, type, handler);
}

/**
 * Validates all params and calls the right
 * listener function based on its target type.
 *
 * @param {String} method
 * @param {String|HTMLElement|HTMLCollection|NodeList} target
 * @param {String} type
 * @param {Function} handler
 * @return {Object}
 */

function event(method, target, type, handler) {

  if (!target && !type && !handler) {
    throw new Error('Missing required arguments');
  }

  if (!is.string(type)) {
    throw new TypeError('Second argument must be a String');
  }

  if (!is.fn(handler) && !is.object(handler)) {
    throw new TypeError('Third argument must be a Function');
  }

  if (is.node(target) || is.object(target)) {
    return listenNode(method, target, type, handler);
  } else if (is.nodeList(target)) {
    return listenNodeList(method, target, type, handler);
  } else if (is.string(target)) {
    var nodes = document.querySelectorAll(target);
    return listenNodeList(method, nodes, type, handler);
  } else {
    throw new TypeError('First argument must be a String, HTMLElement, HTMLCollection, or NodeList');
  }
}

/**
 * Adds an event listener to a HTML element
 *
 * @param {String} method
 * @param {HTMLElement} node
 * @param {String} type
 * @param {Function} handler
 * @return {Object}
 */

function listenNode(method, node, type, handler) {
  node[method](type, handler);
}

/**
 * Add an event listener to a list of HTML elements
 * and returns a remove listener function.
 *
 * @param {String} method
 * @param {NodeList|HTMLCollection} nodes
 * @param {String} type
 * @param {Function} handler
 * @return {Object}
 */

function listenNodeList(method, nodes, type, handler) {
  (0, _foreach2.default)(nodes, function (node) {
    node[method](type, handler);
  });
}